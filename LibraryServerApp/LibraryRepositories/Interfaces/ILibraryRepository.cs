﻿namespace LibraryRepositories.Interfaces
{
    using LibraryRepositories.Models;
    using System.Collections.ObjectModel;

    public interface ILibraryRepository
    {
        void AddBook(string jsonData);

        void EditBook(int index, string jsonData);

        void DeleteBook(int index);

        string GetBookByIndex(int index);

        string GetAllBooks();

        void SaveChanges(ObservableCollection<BookModel> books);
    }
}
