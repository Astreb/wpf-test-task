﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryClient.Interfaces
{
    public interface IActionService
    {
        void DoAction(Task actionTask, string loadingWindowText);

        void DoActionAsync(Task actionTask);
    }
}
