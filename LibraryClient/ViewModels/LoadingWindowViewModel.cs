﻿using LibraryClient.Interfaces;
using LibraryClient.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryClient.ViewModels
{
    public class LoadingWindowViewModel : BaseViewModel<LoadingWindow, LoadingWindowViewModel>
    {
        private string _loadingWindowText;
        public string LoadingWindowText 
        {
            get => _loadingWindowText; 
            set
            {
                _loadingWindowText = value;
                OnPropertyChanged("LoadingWindowText");
            }
        }

        public LoadingWindowViewModel(IMessageService messageService) : base(messageService)
        {
            LoadingWindowText = "";
        }
    }
}
