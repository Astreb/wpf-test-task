﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace LibraryServer.Filters
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            var jsonResult = new JsonResult(exception.Message);
            jsonResult.StatusCode = (int)HttpStatusCode.BadRequest;
            context.Result = jsonResult;
        }
    }
}
