﻿using System;

namespace LibraryClient.DTO
{
    [Serializable]
    public class BookDTO
    {
        public string Name { get; set; }

        public string Photo { get; set; }
    }
}
