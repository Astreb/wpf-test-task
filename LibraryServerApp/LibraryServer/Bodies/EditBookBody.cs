﻿namespace LibraryServer.Bodies
{
    public class EditBookBody
    {
        public int Index { get; set; }

        public string JsonData { get; set; }
    }
}
