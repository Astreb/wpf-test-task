﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryClient.Interfaces
{
    public interface IMessageService
    {
        void ShowMessage(string message);

        void ShowInfoMessage(string message);

        void ShowExceptionMessage(Exception ex);

        bool ShowConfirmationMessage(string message, string title);
    }
}
