﻿using LibraryClient.Interfaces;
using LibraryClient.Services;
using LibraryClient.ViewModels;
using LibraryClient.Views;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows;

namespace LibraryClient
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public ServiceProvider ServiceProvider { get; set; }

        public App()
        {
            InitializeComponent();
        }

        public void InitializeServices()
        {
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            ServiceProvider = services.BuildServiceProvider();
        }

        public void ConfigureServices(ServiceCollection services)
        {
            IConfigService configService = new ConfigService(Application.Current.Resources);

            //Windows
            services.AddSingleton<MainWindow>();

            //Services
            services.AddSingleton(configService);
            services.AddSingleton<LoadingWindowViewModel>();
            services.AddSingleton<IConnectionService, ConnectionService>();
            services.AddSingleton<IActionService, ActionService>();
            services.AddSingleton<IBookService, BookService>();
            services.AddSingleton<IViewModelProviderService, ViewModelProviderService>();
            services.AddSingleton<IMessageService, MessageService>();
            services.AddTransient<ILoadingWindowService, LoadingWindowService>();

            //View models
            services.AddSingleton<MainWindowViewModel>();
            services.AddTransient<AddBookViewModel>();
            services.AddTransient<EditBookViewModel>();
            services.AddTransient<DeleteBookViewModel>();
            services.AddTransient<ShowBookViewModel>();
        }

        [STAThread]
        static void Main()
        {
            App app = new App();

            app.InitializeServices();

            MainWindow mainWindow = app.ServiceProvider.GetService<MainWindow>();
            MainWindowViewModel mainWindowViewModel = app.ServiceProvider.GetService<MainWindowViewModel>();
            mainWindow.DataContext = mainWindowViewModel;

            app.Run(mainWindow);
        }
    }
}
