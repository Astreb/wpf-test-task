﻿using LibraryClient.Interfaces;
using System.Windows;

namespace LibraryClient.Services
{
    public class ConfigService : IConfigService
    {
        #region Properties

        private ResourceDictionary _resourceDictionary;

        #endregion

        #region Constructors

        public ConfigService(ResourceDictionary resourceDictionary)
        {
            _resourceDictionary = resourceDictionary;
        }

        #endregion

        #region Methods

        public string GetResource(string resourceKey)
        {
            return (string)_resourceDictionary[resourceKey];
        }

        #endregion
    }
}
