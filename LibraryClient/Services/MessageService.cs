﻿using LibraryClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LibraryClient.Services
{
    public class MessageService : IMessageService
    {
        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }

        public void ShowInfoMessage(string message)
        {
            ShowMessage(message);
        }

        public void ShowExceptionMessage(Exception ex)
        {
            ShowMessage(ex.Message);
        }

        public bool ShowConfirmationMessage(string message, string title)
        {
            return MessageBox.Show(message, title, MessageBoxButton.YesNoCancel) == MessageBoxResult.Yes;
        }
    }
}
