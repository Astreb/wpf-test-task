﻿namespace LibraryClient.Interfaces
{
    public interface IWindowDialogService
    {
        void CloseDialog();

        void ShowDialog();
    }
}
