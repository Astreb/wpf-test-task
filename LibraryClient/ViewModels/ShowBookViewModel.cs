﻿using LibraryClient.Comands;
using LibraryClient.Interfaces;
using LibraryClient.Models;
using LibraryClient.Views;
using System.Windows.Input;

namespace LibraryClient.ViewModels
{
    public class ShowBookViewModel : BaseViewModel<ShowBookWindow, ShowBookViewModel>
    {
        #region Properties

        public BookModel SelectedBookModel { get; set; }

        #endregion

        #region Commands

        public ICommand EscapeClickCommand { get; set; }

        #endregion

        #region Constructors

        public ShowBookViewModel(MainWindow ownerWindow,
            IMessageService messageService) : base(ownerWindow, messageService)
        {
            EscapeClickCommand = new DelegateCommand(EscapeClickEventHandler);
        }

        #endregion

        #region Initializers

        public void InitializeProperties(BookModel selectedBookModel)
        {
            SelectedBookModel = selectedBookModel;
        }

        #endregion

        #region Command Methods

        public void EscapeClickEventHandler()
        {
            CloseDialogWindow();
        }

        #endregion
    }
}