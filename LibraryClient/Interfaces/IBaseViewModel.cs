﻿using System.ComponentModel;

namespace LibraryClient.Interfaces
{
    public interface IBaseViewModel : INotifyPropertyChanged
    {
        void CreateDialogWindow();

        void ShowDialogWindow();

        void CloseDialogWindow();

        void CloseMainWindow();
    }
}
