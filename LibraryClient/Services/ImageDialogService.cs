﻿using LibraryClient.Interfaces;
using Microsoft.Win32;
using System;

namespace LibraryClient.Services
{
    public class ImageDialogService : IFileDialogService
    {
        #region Properties

        private string _filter;

        private Uri _fileUri;

        #endregion

        #region Constructors

        public ImageDialogService(string filter)
        {
            _filter = filter;
        }

        #endregion

        #region Methods

        public void BrowseFile()
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = _filter;

            if (openDialog.ShowDialog() == true)
            {
                _fileUri = new Uri(openDialog.FileName);
            }
        }

        public Uri GetBrowsedFile()
        {
            return _fileUri;
        }

        #endregion
    }
}
