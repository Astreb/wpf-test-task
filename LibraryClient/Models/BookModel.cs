﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;

namespace LibraryClient.Models
{
    public class BookModel : INotifyPropertyChanged
    {
        private string _name;

        private BitmapImage _photo;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public BitmapImage Photo
        {
            get => _photo;
            set
            {
                _photo = value;
                OnPropertyChanged("Photo");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void Validate()
        {
            if (Name == "" || Photo == null)
            {
                throw new Exception("Некорректная информация о книге!");
            }
        }

        public void OnPropertyChanged([CallerMemberName] string par = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(par));
        }
    }
}
