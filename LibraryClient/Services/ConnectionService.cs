﻿using LibraryClient.Interfaces;
using System;
using System.Net;
using System.Threading.Tasks;

namespace LibraryClient.Services
{
    public class ConnectionService : IConnectionService
    {
        #region Properties

        private readonly string _connectionUrl;
        
        private readonly IConfigService _configService;

        private ILoadingWindowService _loadingWindowService;

        private bool _isConnected;

        #endregion

        #region Constructors

        public ConnectionService(IConfigService configService, ILoadingWindowService loadingWindowService)
        {
            _configService = configService;
            _loadingWindowService = loadingWindowService;
            _connectionUrl = _configService.GetResource("CheckConnectionRequestUrl");
            _isConnected = false;
        }

        #endregion

        #region Methods

        public bool Ping()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(_connectionUrl);
            request.Method = "GET";

            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (WebException)
            {
                return false;
            }

            return response.StatusCode == HttpStatusCode.OK;
        }

        public void CheckConnection()
        {
            CheckConnectionAsync();
            _loadingWindowService.ShowLoadingWindow(_configService.GetResource("LoadingWindowCheckConnectionAction"));

            if (!_isConnected)
            {
                throw new Exception("Нет подключения к серверу.");
            }
        }

        #endregion

        #region Async methods

        public async void CheckConnectionAsync()
        {
            _isConnected = await Task.Run(() => Ping());

            _loadingWindowService.CloseLoadingWindow();
        }

        #endregion
    }
}
