﻿using System;
using System.Threading.Tasks;

namespace LibraryClient.Interfaces
{
    public interface IConnectionService
    {
        bool Ping();

        void CheckConnection();

        void CheckConnectionAsync();
    }
}
