﻿namespace LibraryRepositories.Repositories
{
    using LibraryRepositories.Models;
    using Newtonsoft.Json;
    using System.Collections.ObjectModel;
    using System.IO;

    public static class BookSerealization
    {
        public static BookModel BookDeserealize(string jsonBook)
        {
            return JsonConvert.DeserializeObject<BookModel>(jsonBook);
        }

        public static ObservableCollection<BookModel> BooksCollectionDeserealize(string jsonBooksCollection)
        {
            var bookModelCollection = JsonConvert.DeserializeObject<ObservableCollection<BookModel>>(jsonBooksCollection);
            if (bookModelCollection == null)
            {
                bookModelCollection = new ObservableCollection<BookModel>();
            }

            return bookModelCollection;
        }

        public static string BooksCollectionSerealize(ObservableCollection<BookModel> books)
        {
            return JsonConvert.SerializeObject(books);
        }

        public static string BookSerealize(BookModel bookModel)
        {
            return JsonConvert.SerializeObject(bookModel);
        }

        public static string LoadFromFile()
        {
            string filename = "Books.json";
            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
            }
            using (StreamReader sr = new StreamReader(filename, System.Text.Encoding.Default))
            {
                return sr.ReadToEnd();
            }
        }

        public static void SaveToFile(string jsonData)
        {
            string filename = "Books.json";

            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
            }
            using (StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.Default))
            {
                sw.Write(jsonData);
            }
        }
    }
}
