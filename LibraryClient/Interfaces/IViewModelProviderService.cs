﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryClient.Interfaces
{
    public interface IViewModelProviderService
    {
        TViewModel GetViewModel<TViewModel>() where TViewModel : IBaseViewModel;
    }
}
