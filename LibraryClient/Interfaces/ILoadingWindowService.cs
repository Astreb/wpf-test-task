﻿namespace LibraryClient.Interfaces
{
    public interface ILoadingWindowService
    {
        void ShowLoadingWindow(string loadingWindowText);

        void CloseLoadingWindow();
    }
}
