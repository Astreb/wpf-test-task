﻿using LibraryClient.Interfaces;
using LibraryClient.ViewModels;
using LibraryClient.Views;
using System.Windows;

namespace LibraryClient.Services
{
    public class LoadingWindowService : ILoadingWindowService
    {
        #region Properties

        private LoadingWindow _loadingWindow;

        private MainWindow _ownerWindow;

        private LoadingWindowViewModel _loadingWindowViewModel;

        #endregion

        #region Constructors

        public LoadingWindowService(MainWindow mainWindow, LoadingWindowViewModel loadingWindowViewModel)
        {
            _loadingWindow = new LoadingWindow();
            _ownerWindow = mainWindow;
            _loadingWindowViewModel = loadingWindowViewModel;
        }

        #endregion

        #region Methods

        public void CloseLoadingWindow()
        {
            if (_loadingWindow.IsLoaded)
            {
                _loadingWindow.Hide();
            }
        }

        public void ShowLoadingWindow(string loadingWindowText)
        {
            InitializeLoadingWindow(loadingWindowText);

            _loadingWindow.ShowDialog();
        }

        public void InitializeLoadingWindow(string loadingWindowText)
        {
            SetLoadingWindowText(loadingWindowText);

            _loadingWindow.Owner = _ownerWindow;
            _loadingWindow.DataContext = _loadingWindowViewModel;
        }

        public void SetLoadingWindowText(string loadingWindowText)
        {
            _loadingWindowViewModel.LoadingWindowText = loadingWindowText;
        }

        #endregion
    }
}
