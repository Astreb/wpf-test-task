﻿using LibraryClient.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LibraryClient.Services
{
    public abstract class BaseMessageService
    {
        protected readonly IMessageService _messageService;

        public BaseMessageService(IMessageService messageService)
        {
            _messageService = messageService;
        }

        protected void ShowMessage(string message)
        {
            _messageService.ShowMessage(message);
        }

        public void ShowInfoMessage(string message)
        {
            _messageService.ShowInfoMessage(message);
        }

        public void ShowExceptionMessage(Exception ex)
        {
            _messageService.ShowExceptionMessage(ex);
        }

        public bool ShowConfirmationMessage(string message, string title)
        {
            return _messageService.ShowConfirmationMessage(message, title);
        }
    }
}
