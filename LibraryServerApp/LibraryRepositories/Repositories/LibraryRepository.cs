﻿namespace LibraryRepositories.Repositories
{
    using LibraryRepositories.Interfaces;
    using LibraryRepositories.Models;
    using System.Collections.ObjectModel;

    public class LibraryRepository : ILibraryRepository
    {
        public LibraryRepository()
        {
        }

        public void AddBook(string jsonData)
        {
            BookModel book = BookSerealization.BookDeserealize(jsonData);
            jsonData = GetAllBooks();
            ObservableCollection<BookModel> books = BookSerealization.BooksCollectionDeserealize(jsonData);
            
            books.Add(book);
            
            SaveChanges(books);
        }

        public void EditBook(int index, string jsonData)
        {
            BookModel book = BookSerealization.BookDeserealize(jsonData);
            jsonData = GetAllBooks();
            ObservableCollection<BookModel> books = BookSerealization.BooksCollectionDeserealize(jsonData);

            books[index].Name = book.Name;
            books[index].Photo = book.Photo;

            SaveChanges(books);
        }

        public void DeleteBook(int index)
        {
            string jsonData = GetAllBooks();
            ObservableCollection<BookModel> books = BookSerealization.BooksCollectionDeserealize(jsonData);

            books.RemoveAt(index);

            SaveChanges(books);
        }

        public string GetBookByIndex(int index)
        {
            string jsonData = GetAllBooks();
            ObservableCollection<BookModel> books = BookSerealization.BooksCollectionDeserealize(jsonData);

            return BookSerealization.BookSerealize(books[index]);
        }

        public string GetAllBooks()
        {
            return BookSerealization.LoadFromFile(); ;
        }

        public void SaveChanges(ObservableCollection<BookModel> books)
        {
            string jsonData = BookSerealization.BooksCollectionSerealize(books);
            BookSerealization.SaveToFile(jsonData);
        }
    }
}
