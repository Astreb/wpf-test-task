﻿// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LibraryServer.Controllers
{

    using LibraryRepositories.Interfaces;
    using LibraryServer.Bodies;
    using LibraryServer.Filters;
    using Microsoft.AspNetCore.Mvc;
    using System;

    [Route("api/[controller]")]
    [ApiController]
    public class LibraryController : ControllerBase
    {
        private readonly ILibraryRepository libraryRepository;

        public LibraryController(ILibraryRepository libraryRepository)
        {
            this.libraryRepository = libraryRepository;
        }

        [HttpGet("")]
        public IActionResult CheckConnection()
        {
            return StatusCode(200);
        }

        [HttpGet("books/get/all")]
        [CustomExceptionFilter]
        public string GetAllBooks()
        {
            return libraryRepository.GetAllBooks();
        }

        [HttpGet("books/get/{index}")]
        [CustomExceptionFilter]
        public string GetBookByIndex(int index)
        {
            return libraryRepository.GetBookByIndex(index);
        }

        [HttpPost("books/add")]
        [CustomExceptionFilter]
        public IActionResult AddBook([FromBody]AddBookBody addBookBody)
        {
            if (addBookBody.JsonData.Contains("LOLKA"))
                throw new Exception("Имя LOLKA не допустимо!!!!!");

            libraryRepository.AddBook(addBookBody.JsonData);
            return Ok();
            
        }

        [HttpPut("books/edit")]
        [CustomExceptionFilter]
        public IActionResult EditBook([FromBody] EditBookBody editBookBody)
        {
            try
            {
                libraryRepository.EditBook(editBookBody.Index, editBookBody.JsonData);
                return Ok();
            }
            catch (Exception ex)
            {
                return (IActionResult)ex;
            }
        }

        [HttpDelete("books/delete")]
        [CustomExceptionFilter]
        public IActionResult DeleteBook(int index)
        {
            try
            {
                libraryRepository.DeleteBook(index);
                return Ok();
            }
            catch (Exception ex)
            {
                return (IActionResult)ex;
            }
        }
    }
}
