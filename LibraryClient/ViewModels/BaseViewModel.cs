﻿using LibraryClient.Interfaces;
using LibraryClient.Services;
using LibraryClient.Views;
using Microsoft.Extensions.DependencyInjection;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace LibraryClient.ViewModels
{
    public abstract class BaseViewModel<TWindow, TViewModel> : BaseMessageService, IBaseViewModel where TWindow : Window, new()
                                                                                                  where TViewModel : IBaseViewModel
    {
        #region Properties

        protected Window _mainWindow;

        protected IWindowDialogService _dialogService;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors

        public BaseViewModel(IMessageService messageService) : base(messageService)
        { }

        public BaseViewModel(MainWindow mainWindow,
            IMessageService messageService) : base(messageService)
        {
            _mainWindow = mainWindow;
        }

        #endregion

        #region Methods

        public void CreateDialogWindow()
        {
            TWindow dialogWindow = new TWindow();
            _dialogService = new WindowDialogService(_mainWindow, dialogWindow, this);
        }

        public void ShowDialogWindow()
        {
            CreateDialogWindow();
            _dialogService?.ShowDialog();
        }

        public void CloseDialogWindow()
        {
            _dialogService?.CloseDialog();
        }

        public void CloseMainWindow()
        {
            _mainWindow.Close();
        }

        #endregion

        #region Property changed methods

        public void OnPropertyChanged([CallerMemberName] string par = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(par));
        }

        #endregion
    }
}
