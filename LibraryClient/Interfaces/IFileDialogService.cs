﻿using System;

namespace LibraryClient.Interfaces
{
    public interface IFileDialogService
    {
        void BrowseFile();

        Uri GetBrowsedFile();
    }
}
