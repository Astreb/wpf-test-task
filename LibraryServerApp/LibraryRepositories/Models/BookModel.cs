﻿namespace LibraryRepositories.Models
{
    using System;

    [Serializable]
    public class BookModel
    {
        public string Name { get; set; }

        public string Photo { get; set; }
    }
}
