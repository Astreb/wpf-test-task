﻿using LibraryClient.Comands;
using LibraryClient.Interfaces;
using LibraryClient.Views;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace LibraryClient.ViewModels
{
    public class DeleteBookViewModel : BaseViewModel<DeleteBookWindow, DeleteBookViewModel>
    {
        #region Services

        private readonly IActionService _actionService;

        private readonly IConfigService _configService;

        public IBookService BookService { get; set; }

        #endregion

        #region Properties

        private int _selectedBookIndex;
        public int SelectedBookIndex
        {
            get => _selectedBookIndex;
            set
            {
                _selectedBookIndex = value;
                OnPropertyChanged("SelectedBookIndex");
            }
        }

        #endregion

        #region Commands

        public ICommand YesCommand { get; set; }

        public ICommand NoCommand { get; set; }

        #endregion

        #region Constructors

        public DeleteBookViewModel(MainWindow ownerWindow,
            IMessageService messageService,
            IConfigService configService,
            IActionService actionService,
            IBookService bookService) : base(ownerWindow, messageService)
        {
            _configService = configService;
            _actionService = actionService;
            BookService = bookService;

            InitializeCommands();
        }

        #endregion

        #region Initializers

        public void InitializeProperties(int selectedBookIndex)
        {
            SelectedBookIndex = selectedBookIndex;
        }

        public void InitializeCommands()
        {
            YesCommand = new DelegateCommand(YesClick);
            NoCommand = new DelegateCommand(NoClick);
        }

        #endregion

        #region Command Methods

        public void YesClick()
        {
            try
            {
                Task deleteBookTask = new Task(() => BookService.DeleteBook(SelectedBookIndex));
                _actionService.DoAction(deleteBookTask, _configService.GetResource("LoadingWindowSaveAction"));

                ShowInfoMessage("Книга успешно удалена.");
                CloseDialogWindow();
            }
            catch (Exception ex)
            {
                ShowExceptionMessage(ex);
            }
        }

        public void NoClick()
        {
            string message = "Вы действительно хотите отменить удаление книги?";
            string title = "Отмена удаления";
            if (ShowConfirmationMessage(message, title))
            {
                CloseDialogWindow();
            }
        }

        #endregion
    }
}
