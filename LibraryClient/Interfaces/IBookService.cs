﻿using LibraryClient.Models;

namespace LibraryClient.Interfaces
{
    public interface IBookService
    {
        void AddBook(BookModel bookModel);

        void EditBook(int index, BookModel book);

        void DeleteBook(int index);

        BookModel GetBookDTOByIndex(int index);

        void GetAllBooks();
    }
}
