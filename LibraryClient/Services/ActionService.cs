﻿using LibraryClient.Interfaces;
using System;
using System.Threading.Tasks;

namespace LibraryClient.Services
{
    public class ActionService : IActionService
    {
        #region Properties

        private readonly IConnectionService _connectionService;

        private readonly ILoadingWindowService _loadingWindowService;

        #endregion

        #region Constructors

        public ActionService(IConnectionService connectionService, ILoadingWindowService loadingWindowService)
        {
            _connectionService = connectionService;
            _loadingWindowService = loadingWindowService;
        }

        #endregion

        #region Do action method

        public void DoAction(Task actionTask, string loadingWindowText)
        {
            try
            {
                //Проверка подключения к серверу
                _connectionService.CheckConnection();

                DoActionAsync(actionTask);
                _loadingWindowService.ShowLoadingWindow(loadingWindowText);
            }
            catch (Exception ex)
            {
                _loadingWindowService.CloseLoadingWindow();
                throw ex;
            }
        }

        #endregion

        #region Async methods

        public async void DoActionAsync(Task actionTask)
        {
            actionTask.Start();
            await actionTask;

            _loadingWindowService.CloseLoadingWindow();
        }

        #endregion
    }
}