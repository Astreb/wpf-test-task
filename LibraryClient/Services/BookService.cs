﻿using LibraryClient.Interfaces;
using LibraryClient.Models;
using LibraryClient.Serializers;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Runtime.CompilerServices;
using System.Web.Script.Serialization;

namespace LibraryClient.Services
{
    public class BookService : IBookService, INotifyPropertyChanged
    {
        #region Properties

        public string ContentType = "application/json";

        private ObservableCollection<BookModel> _books;
        public ObservableCollection<BookModel> Books
        {
            get => _books;
            set
            {
                _books = value;
                OnPropertyChanged("Books");
            }
        }

        #endregion

        #region Services

        private readonly IConfigService _configService;

        #endregion

        public BookService(IConfigService configService)
        {
            _configService = configService;

            Books = new ObservableCollection<BookModel>();
        }

        #region Actions

        public void AddBook(BookModel bookModel)
        {
            var jsonData = BookSerealization.BookModelSerealize(bookModel);

            string address = _configService.GetResource("AddBookRequestUrl");
            string httpRequestMethod = "POST";
            string contentType = ContentType;
            jsonData = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue }
                .Serialize(new
                {
                    JsonData = jsonData
                });

            HttpWebRequest request = CreateHttpWebRequest(address, httpRequestMethod, data: jsonData, contentType: contentType);
            GetHttpWebResponse(request);

            GetAllBooks();
        }

        public void EditBook(int index, BookModel book)
        {
            Books[index].Name = book.Name;
            Books[index].Photo = book.Photo;

            var jsonData = BookSerealization.BookModelSerealize(Books[index]);

            string address = _configService.GetResource("EditBookRequestUrl");
            string httpRequestMethod = "PUT";
            string contentType = ContentType;
            jsonData = new JavaScriptSerializer() { MaxJsonLength = int.MaxValue }
                .Serialize(new
                {
                    Index = index,
                    JsonData = jsonData
                });

            HttpWebRequest request = CreateHttpWebRequest(address, httpRequestMethod, data:jsonData, contentType:contentType);
            GetHttpWebResponse(request);

            GetAllBooks();
        }

        public void DeleteBook(int index)
        {
            string address = _configService.GetResource("DeleteBookRequestUrl");
            string parameters = String.Format("?index={0}", index);
            string httpRequestMethod = "DELETE";

            HttpWebRequest request = CreateHttpWebRequest(address, httpRequestMethod, parameters);
            GetHttpWebResponse(request);

            GetAllBooks();
        }

        public BookModel GetBookDTOByIndex(int index)
        {
            string address = _configService.GetResource("GetBookByIdRequestUrl");
            string parameters = String.Format("/{0}", index);
            string httpRequestMethod = "GET";

            HttpWebRequest request = CreateHttpWebRequest(address, httpRequestMethod, parameters);
            string response = GetHttpWebResponse(request);

            return BookSerealization.BookDTODeserealize(response);
        }

        public void GetAllBooks()
        {
            string address = _configService.GetResource("GetAllBooksRequestUrl");
            string httpRequestMethod = "GET";

            HttpWebRequest request = CreateHttpWebRequest(address, httpRequestMethod);
            var response = GetHttpWebResponse(request);

            Books = BookSerealization.BooksCollectionDeserealize(response);
        }

        #endregion

        #region Reques and response methods

        public HttpWebRequest CreateHttpWebRequest(string address, string httpRequestMethod, string parameters = "", string data = "", string contentType = "")
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address + parameters);
            request.Method = httpRequestMethod;

            if (contentType != "")
            {
                request.ContentType = contentType;
            }

            if (data != "")
            {
                using (StreamWriter dataStream = new StreamWriter(request.GetRequestStream()))
                {
                    dataStream.Write(data);
                }
            }

            return request;
        }

        public string GetHttpWebResponse(HttpWebRequest request)
        {
            try
            {
                string jsonData;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        jsonData = reader.ReadToEnd();
                    }
                }
                response.Close();

                return jsonData;
            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    using (Stream data = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(data))
                        {
                            string text = reader.ReadToEnd();

                            throw new Exception(text, ex);
                        }
                    }
                }
            }
        }

        #endregion

        #region Property Changed

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string par = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(par));
        }

        #endregion
    }
}
