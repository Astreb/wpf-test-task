﻿namespace LibraryClient.Interfaces
{
    public interface IConfigService
    {
        string GetResource(string resourceKey);
    }
}
