﻿using System;
using System.Windows.Input;

namespace LibraryClient.Comands
{
    public class DelegateCommandWithCondition : ICommand
    {
        private readonly Action _executeCommand;
        private readonly Func<object, bool> _canExecuteCondition;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public DelegateCommandWithCondition(Action executeCommand, Func<object, bool> canExecuteCondition)
        {
            _executeCommand = executeCommand;
            _canExecuteCondition = canExecuteCondition;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecuteCondition(parameter);
        }

        public void Execute(object parameter)
        {
            _executeCommand();
        }
    }
}
