﻿using LibraryClient.Converters;
using LibraryClient.DTO;
using LibraryClient.Models;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.IO;

namespace LibraryClient.Serializers
{
    public static class BookSerealization
    {
        public static BookModel BookDTODeserealize(string jsonBook)
        {
            BookDTO bookDTO = JsonConvert.DeserializeObject<BookDTO>(jsonBook);
            BookModel bookModel = BookConverter.FromBookDTOToBookModel(bookDTO);

            return bookModel;
        }

        public static ObservableCollection<BookModel> BooksCollectionDeserealize(string jsonBooksCollection)
        {
            ObservableCollection<BookDTO> booksDTOCollection = JsonConvert.DeserializeObject<ObservableCollection<BookDTO>>(jsonBooksCollection);
            if (booksDTOCollection == null)
            {
                booksDTOCollection = new ObservableCollection<BookDTO>();
            }

            ObservableCollection<BookModel> bookModelsCollection = new ObservableCollection<BookModel>();

            foreach (BookDTO bookDTO in booksDTOCollection)
            {
                BookModel bookModel = BookConverter.FromBookDTOToBookModel(bookDTO);
                bookModelsCollection.Add(bookModel);
            }

            return bookModelsCollection;
        }

        public static string BookModelsCollectionSerealize(ObservableCollection<BookModel> bookModels)
        {
            ObservableCollection<BookDTO> bookDTOCollection = new ObservableCollection<BookDTO>();

            foreach (BookModel bookModel in bookModels)
            {
                BookDTO bookDTO = BookConverter.FromBookModelToBookDTO(bookModel);
                bookDTOCollection.Add(bookDTO);
            }

            return JsonConvert.SerializeObject(bookDTOCollection);
        }

        public static string BookModelSerealize(BookModel bookModel)
        {
            BookDTO bookDTO = BookConverter.FromBookModelToBookDTO(bookModel);
            string jsonBook = JsonConvert.SerializeObject(bookDTO);

            return jsonBook;
        }

        public static string LoadFromFile()
        {
            string filename = "Books.json";
            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
            }
            using (StreamReader sr = new StreamReader(filename, System.Text.Encoding.Default))
            {
                return sr.ReadToEnd();
            }
        }

        public static void SaveToFile(string jsonData)
        {
            string filename = "Books.json";
            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
            }
            using (StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.Default))
            {
                sw.Write(jsonData);
            }
        }
    }
}
