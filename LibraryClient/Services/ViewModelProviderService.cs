﻿using LibraryClient.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LibraryClient.Services
{
    public class ViewModelProviderService : IViewModelProviderService
    {
        private readonly ServiceProvider _serviceProvider;

        public ViewModelProviderService()
        {
            _serviceProvider = ((App)Application.Current).ServiceProvider;
        }

        public TViewModel GetViewModel<TViewModel>() where TViewModel : IBaseViewModel
        {
            return _serviceProvider.GetService<TViewModel>();
        }
    }
}
