﻿using LibraryClient.Comands;
using LibraryClient.Interfaces;
using LibraryClient.Models;
using LibraryClient.Services;
using LibraryClient.Views;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace LibraryClient.ViewModels
{
    public class AddBookViewModel : BaseViewModel<AddBookWindow, AddBookViewModel>
    {
        #region Services

        private readonly IActionService _actionService;

        private readonly IConfigService _configService;

        private readonly IFileDialogService _imageDialogService;

        public IBookService BookService { get; set; }

        #endregion

        #region Properties

        public BookModel BookModel { get; set; }

        #endregion

        #region Commands

        public ICommand SaveBookCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ICommand AddBookPhotoCommand { get; set; }

        #endregion

        #region Constructors

        public AddBookViewModel(MainWindow ownerWindow,
            IMessageService messageService,
            IConfigService configService,
            IActionService actionService,
            IBookService bookService) : base(ownerWindow, messageService)
        {
            _configService = configService;
            _actionService = actionService;
            _imageDialogService = new ImageDialogService(configService.GetResource("ImageFilter"));

            BookService = bookService;
            BookModel = new BookModel();

            InitializeCommands();
        }

        #endregion

        #region Initializers

        public void InitializeCommands()
        {
            SaveBookCommand = new DelegateCommand(SaveBook);
            CancelCommand = new DelegateCommand(CancelClick);
            AddBookPhotoCommand = new DelegateCommand(AddBookPhoto);
        }

        #endregion

        #region Command Methods

        public void SaveBook()
        {
            try
            {
                BookModel.Validate();

                Task addBookTask = new Task(() => BookService.AddBook(BookModel));
                _actionService.DoAction(addBookTask, _configService.GetResource("LoadingWindowSaveAction"));

                ShowInfoMessage("Книга успешно добавлена.");
                CloseDialogWindow();
            }
            catch (Exception ex)
            {
                ShowExceptionMessage(ex);
            }
        }

        public void CancelClick()
        {
            string message = "Вы действительно хотите отменить добавление книги?";
            string title = "Отмена добавления";
            if (ShowConfirmationMessage(message, title))
            {
                CloseDialogWindow();
            }
        }

        public void AddBookPhoto()
        {
            try
            {
                _imageDialogService.BrowseFile();
                Uri fileUri = _imageDialogService.GetBrowsedFile();
                if (fileUri != null)
                {
                    BookModel.Photo = new BitmapImage(_imageDialogService.GetBrowsedFile());
                }
            }
            catch (Exception ex)
            {
                ShowExceptionMessage(ex);
            }
        }

        #endregion
    }
}
