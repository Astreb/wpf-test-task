﻿using System;
using System.Windows.Input;

namespace LibraryClient.Comands
{
    public class DelegateCommand : ICommand
    {
        private readonly Action _executeCommand;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public DelegateCommand(Action executeCommand)
        {
            this._executeCommand = executeCommand;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _executeCommand();
        }
    }
}
