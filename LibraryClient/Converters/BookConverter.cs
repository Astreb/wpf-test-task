﻿using LibraryClient.DTO;
using LibraryClient.Models;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace LibraryClient.Converters
{
    public static class BookConverter
    {
        private static string FromBitmapImageToBase64(BitmapImage bitmapImage)
        {
            Bitmap bitmap;

            using (var outStream = new MemoryStream())
            {
                var enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                bitmap = new Bitmap(outStream);

                var ms = new MemoryStream();
                bitmap.Save(ms, bitmap.RawFormat);
                byte[] imageBytes = ms.ToArray();
                return Convert.ToBase64String(imageBytes);
            }
        }

        private static BitmapImage FromBase64ToBitmapImage(string bookModelPhoto)
        {
            using (var memory = new MemoryStream())
            {
                var bitmap = Bitmap.FromStream(new MemoryStream(Convert.FromBase64String(bookModelPhoto)));

                bitmap.Save(memory, ImageFormat.Jpeg);
                memory.Position = 0;

                var bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memory;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze();

                return bitmapImage;
            }
        }

        public static BookModel FromBookDTOToBookModel(BookDTO bookDTO)
        {
            BookModel bookModel = new BookModel();
            bookModel.Name = bookDTO.Name;
            bookModel.Photo = BookConverter.FromBase64ToBitmapImage(bookDTO.Photo);

            return bookModel;
        }

        public static BookDTO FromBookModelToBookDTO(BookModel bookModel)
        {
            BookDTO bookDTO = new BookDTO();
            bookDTO.Name = bookModel.Name;
            bookDTO.Photo = BookConverter.FromBitmapImageToBase64(bookModel.Photo);

            return bookDTO;
        }
    }
}
