﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using LibraryClient.Comands;
using LibraryClient.Interfaces;
using LibraryClient.Models;
using LibraryClient.Services;
using LibraryClient.Views;
using Microsoft.Extensions.DependencyInjection;

namespace LibraryClient.ViewModels
{
    public class MainWindowViewModel : BaseViewModel<MainWindow, MainWindowViewModel>
    {
        #region Services

        private readonly IConfigService _configService;

        private readonly IActionService _actionService;

        private readonly IViewModelProviderService _viewModelProvider;

        public IBookService BookService { get; set; }

        #endregion

        #region Properties

        private int _selectedBookIndex;
        public int SelectedBookIndex
        {
            get => _selectedBookIndex;
            set
            {
                _selectedBookIndex = value;
                OnPropertyChanged("SelectedBookIndex");
            }
        }

        private BookModel _selectedBookModel;
        public BookModel SelectedBookModel
        {
            get => _selectedBookModel;
            set
            {
                _selectedBookModel = value;
                OnPropertyChanged("SelectedBookModel");
            }
        }

        #endregion

        #region Commands

        public ICommand LoadDataCommand { get; set; }

        public ICommand AddBookCommand { get; set; }

        public ICommand EditBookCommand { get; set; }

        public ICommand DeleteBookCommand { get; set; }

        public ICommand EscapeClickCommand { get; set; }

        public ICommand ExitCommand { get; set; }

        public ICommand ShowBookCommand { get; set; }

        #endregion

        #region Constructors

        public MainWindowViewModel(MainWindow mainWindow,
            IMessageService messageService,
            IConfigService configService,
            IActionService actionService,
            IViewModelProviderService viewModelProvider,
            IBookService bookService) : base(messageService)
        {
            _mainWindow = mainWindow;
            _configService = configService;
            _actionService = actionService;
            _viewModelProvider = viewModelProvider;

            BookService = bookService;

            InitializeCommands();
        }

        #endregion

        #region Initialization

        public void InitializeCommands()
        {
            LoadDataCommand = new DelegateCommand(LoadData);
            AddBookCommand = new DelegateCommand(AddBook);
            EditBookCommand = new DelegateCommandWithCondition(EditBook, (obj) => SelectedBookModel != null);
            DeleteBookCommand = new DelegateCommandWithCondition(DeleteBook, (obj) => SelectedBookModel != null);
            ShowBookCommand = new DelegateCommandWithCondition(ShowBook, (obj) => SelectedBookModel != null);
            EscapeClickCommand = new DelegateCommand(EscapeClickEventHandler);
            ExitCommand = new DelegateCommand(Exit);
        }

        public void LoadData()
        {
            try
            {
                _actionService.DoAction(new Task(() => BookService.GetAllBooks()), _configService.GetResource("LoadingWindowLoadAction"));
                SelectedBookIndex = -1;
                SelectedBookModel = null;
            }
            catch (Exception ex)
            {
                ShowExceptionMessage(ex);
            }
        }

        #endregion

        #region Command methods

        public void AddBook()
        {
            AddBookViewModel addBookViewModel = _viewModelProvider.GetViewModel<AddBookViewModel>();
            addBookViewModel.ShowDialogWindow();
        }

        public void EditBook()
        {
            EditBookViewModel editBookViewModel = _viewModelProvider.GetViewModel<EditBookViewModel>();
            editBookViewModel.InitializeProperties(SelectedBookIndex, SelectedBookModel);
            editBookViewModel.ShowDialogWindow();
        }

        public void DeleteBook()
        {
            DeleteBookViewModel deleteBookViewModel = _viewModelProvider.GetViewModel<DeleteBookViewModel>();
            deleteBookViewModel.InitializeProperties(SelectedBookIndex);
            deleteBookViewModel.ShowDialogWindow();
        }

        public void ShowBook()
        {
            ShowBookViewModel showBookViewModel = _viewModelProvider.GetViewModel<ShowBookViewModel>();
            showBookViewModel.InitializeProperties(SelectedBookModel);
            showBookViewModel.ShowDialogWindow();
        }

        public void EscapeClickEventHandler()
        {
            if (SelectedBookIndex != -1)
            {
                SelectedBookIndex = -1;
            }
            else
            {
                Exit();
            }
        }

        public void Exit()
        {
            string message = "Вы действительно хотите выйти из приложения?";
            string title = "Выход";
            if (ShowConfirmationMessage(message, title))
            {
                CloseMainWindow();
            }
        }

        #endregion
    }
}
