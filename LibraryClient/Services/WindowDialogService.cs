﻿using LibraryClient.Interfaces;
using LibraryClient.ViewModels;
using System.Windows;

namespace LibraryClient.Services
{
    public class WindowDialogService : IWindowDialogService
    {
        #region Properties

        private readonly Window _dialogWindow;

        public IBaseViewModel ViewModel { get; set; }

        #endregion

        #region Constructors

        public WindowDialogService(Window ownerWindow,
            Window dialogWindow,
            IBaseViewModel viewModel)
        {
            ViewModel = viewModel;

            _dialogWindow = dialogWindow;
            dialogWindow.Owner = ownerWindow;
            dialogWindow.DataContext = ViewModel;
        }

        #endregion

        #region Methods

        public void CloseDialog()
        {
            _dialogWindow.DialogResult = true;
        }

        public void ShowDialog()
        {
            _dialogWindow.ShowDialog();
        }

        #endregion
    }
}
