﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryClient.Enums
{
    public enum ActionType
    {
        Get = 0,
        Add = 1,
        Edit = 2,
        Delete = 3
    }
}
