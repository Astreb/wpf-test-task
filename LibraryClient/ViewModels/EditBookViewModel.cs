﻿using LibraryClient.Comands;
using LibraryClient.Interfaces;
using LibraryClient.Models;
using LibraryClient.Services;
using LibraryClient.Views;
using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace LibraryClient.ViewModels
{
    public class EditBookViewModel : BaseViewModel<EditBookWindow, EditBookViewModel>
    {
        #region Services

        private readonly IActionService _actionService;

        private readonly IConfigService _configService;

        private readonly IFileDialogService _imageDialogService;

        public IBookService BookService { get; set; }

        #endregion

        #region Properties

        private int _selectedBookIndex;
        public int SelectedBookIndex
        {
            get => _selectedBookIndex;
            set
            {
                _selectedBookIndex = value;
                OnPropertyChanged("SelectedBookIndex");
            }
        }

        private BookModel _selectedBookModel;
        public BookModel SelectedBookModel
        {
            get => _selectedBookModel;
            set
            {
                _selectedBookModel = value;
                OnPropertyChanged("SelectedBookModel");
            }
        }

        #endregion

        #region Commands

        public ICommand SaveBookCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public ICommand ChangeBookPhotoCommand { get; set; }

        #endregion

        #region Constructors

        public EditBookViewModel(MainWindow ownerWindow,
            IMessageService messageService,
            IConfigService configService,
            IActionService actionService,
            IBookService bookService) : base(ownerWindow, messageService)
        {
            _configService = configService;
            _actionService = actionService;
            _imageDialogService = new ImageDialogService(configService.GetResource("ImageFilter"));
            BookService = bookService;

            SelectedBookModel = new BookModel();

            InitializeCommands();
        }

        #endregion

        #region Initializers

        public void InitializeProperties(int selectedBookIndex,
            BookModel selectedBookModel)
        {
            SelectedBookIndex = selectedBookIndex;
            SelectedBookModel = selectedBookModel;
        }

        public void InitializeCommands()
        {
            SaveBookCommand = new DelegateCommand(SaveBook);
            CancelCommand = new DelegateCommand(CancelClick);
            ChangeBookPhotoCommand = new DelegateCommand(ChangeBookPhoto);
        }

        #endregion

        #region Command methods

        public void SaveBook()
        {
            try
            {
                SelectedBookModel.Validate();

                Task editBookTask = new Task(() => BookService.EditBook(SelectedBookIndex, SelectedBookModel));
                _actionService.DoAction(editBookTask, _configService.GetResource("LoadingWindowSaveAction"));

                ShowInfoMessage("Изменения успешно сохранены.");
                CloseDialogWindow();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message);
                return;
            }
        }

        public void CancelClick()
        {
            string message = "Вы действительно хотите отменить изменение книги?";
            string title = "Отмена";
            if (ShowConfirmationMessage(message, title))
            {
                CloseDialogWindow();
            }
        }

        public void ChangeBookPhoto()
        {
            try
            {
                _imageDialogService.BrowseFile();
                var fileUri = _imageDialogService.GetBrowsedFile();
                if (fileUri != null)
                {
                    SelectedBookModel.Photo = new BitmapImage(_imageDialogService.GetBrowsedFile());
                }
            }
            catch (Exception ex)
            {
                ShowExceptionMessage(ex);
            }
        }

        #endregion
    }
}
